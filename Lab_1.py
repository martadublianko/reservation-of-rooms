import calendar
import os
import pickle
from datetime import datetime
import random

GLOBAL_ROOMS = []


def add_room(room_num):
    GLOBAL_ROOMS.append(Room(str(room_num)))


def add_room_test():
    rooms = ["2", "15", "28", "41", "54", "67", "80", "93"]
    for elem in rooms:
        GLOBAL_ROOMS.append(Room(elem))


def see_rooms():
    for elem in GLOBAL_ROOMS:
        elem.get_num(True)
    print("")


def check_room(number):
    if GLOBAL_ROOMS.count(number):
        return True
    else:
        return False


def set_date():
    # Місяць
    while True:
        for num in range(1, 13):
            print(num, end=" ")
        month_input = int(input("\nМісяць - номер: "))

        if month_input >= datetime.now().month:
            # День
            while True:
                print(calendar.month(datetime.now().year, month_input))
                day_input = int(input("День: "))
                # Перевірка щоб день був в майбутньому
                if (day_input >= datetime.now().day and month_input == datetime.now().month) \
                        or (month_input != datetime.now().month):
                    # перевірка
                    if month_input == datetime.now().month \
                            and day_input == datetime.now().day \
                            and datetime.now().hour > 17:
                        print("Робочий день закінчився.")
                        continue
                    # Введення годин
                    while True:
                        time_h_input = int(input("Години (з 8 до 18): "))
                        if 8 <= time_h_input < 18:
                            # Введення хвилин
                            while True:
                                time_m_input = int(input("Хвилини (0-59): "))
                                if 0 <= time_m_input < 60:
                                    # повертаємо datetime
                                    return datetime(datetime.now().year, month_input, day_input,
                                                    time_h_input, time_m_input)
        else:
            print(f"Зараз не має можливості броні на {month_input}, тільки після {datetime.now().month}")


def randoms(num: int = 5):
    for _ in range(num):
        GLOBAL_ROOMS[random.randint(0, len(GLOBAL_ROOMS) - 1)].add_reservation(
            datetime(datetime.now().year, random.randint(1, 12), random.randint(1, 28),
                     random.randint(8, 18), random.randint(0, 59)),
            "".join([random.choice('абвгґдеєжзиіїйклмнопрстуфхцчшщьюя') for _ in range(10)]))


class Room:
    def __init__(self, room_num):
        self._room_num = str(room_num)
        self._reservations = []

    def __eq__(self, other):
        if self._room_num == other:
            return True
        return False

    def __str__(self):
        return self._room_num

    def add_reservation(self, date, title: str = "Нарада"):
        self._reservations.append(Calendar(date, title))

    def get_num(self, output=False):
        if output:
            print(self._room_num, end=" ")
        else:
            return self._room_num

    def get_calendar(self):
        for _ in self._reservations:
            _.get_date()

    def get_info(self, output=False):
        if output:
            for _ in self._reservations:
                print(_.get_info())
        else:
            return self._reservations


class Calendar:
    def __init__(self, date: datetime, title: str = "Нарада"):
        self._title = title
        self._date = date

    def get_date(self):
        return f"Рік: {self._date.year} міс.: {self._date.month} день: {self._date.day}"

    def get_title(self):
        return self._title

    def get_info(self):
        return f"Назва: {self._title}\n" \
               f"Рік: {self._date.year}\n" \
               f"Міс.: {self._date.month}\n" \
               f"День: {self._date.day}\n" \
               f"Час: {self._date.hour}:{self._date.minute}\n" \
               f"-------------------------"

    def set_title(self, title):
        self._title = title

    def set_date(self, date):
        self._date = date


def delete():
    os.system('cls')
    input_var_room = str(input("1. Кімнату\n2. Запис\n>"))
    see_rooms()
    room_var_input = str(input("В якій кімнаті? > "))
    if "1" == input_var_room:
        if GLOBAL_ROOMS.count(room_var_input) != 0:
            GLOBAL_ROOMS.pop(GLOBAL_ROOMS.index(room_var_input))
            print("Кімната", room_var_input, "видалена!")
            return True
        else:
            print("Такої кімнати не знайдено!")
    elif "2" == input_var_room:

        if GLOBAL_ROOMS.count(room_var_input) != 0:
            num = 1
            task_arr: list = GLOBAL_ROOMS[GLOBAL_ROOMS.index(room_var_input)].get_info()
            for task in task_arr:
                print(f"{num}. {task.get_title()}")
                num += 1
            task_var = int(input("Яку задачу видаляємо? > "))
            task_arr.pop(task_var-1)
            return True
        else:
            print("Такої кімнати не знайдено!")
    return False


# Test
if __name__ == '__main__':
    try:
        os.system('cls')
        add_room_test()
        menu = ["1. Перегляд", "2. Додати", "3. Видалити", "4. Авто-додавання", "5. Зберегти", "6. Відкрити", "7. Вийти"]
        while True:
            print("Меню:\n", "\n".join(menu), sep='')
            input_var = str(input(">"))
            if "1" == input_var:
                print("-" * 21)
                for room in GLOBAL_ROOMS:
                    room.get_info()
                    if len(room.get_info()) != 0:
                        print(" --- ", str(room.get_num()), " --- ")
                        room.get_info(True)

            elif "2" == input_var:
                input_var = str(input("1. Кімнати\n2. Планування \n>"))
                see_rooms()
                room_var = input("Кімната > ")
                if input_var == "1":
                    if GLOBAL_ROOMS.count(room_var) <= 0:
                        add_room(room_var)
                elif input_var == "2":
                    GLOBAL_ROOMS[GLOBAL_ROOMS.index(input_var)].add_reservation(set_date(), input("Помітка: "))

            elif "3" == input_var:
                delete()

            elif "4" == input_var:
                randoms(int(input("Скільки? > ")))
                print("Створено!")
            elif "5" == input_var:
                with open('data.pickle', 'wb') as f:
                    pickle.dump(GLOBAL_ROOMS, f)
                print("Збережено :3")
            elif "6" == input_var:
                with open('data.pickle', 'rb') as f:
                    GLOBAL_ROOMS = pickle.load(f)
                print("Завантажено дані!")
            elif "7" == input_var:
                print("Вихід")
                exit()
            else:
                print("Не маю такого!")

            input("\nДля продовження натисніть Enter")
            os.system('cls')
    except BaseException:
        print("Вихід")
        exit()
